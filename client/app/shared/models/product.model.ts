export class Product {
    _id?: string;
    name?: String;
    description?: String;
    price?: Number;
    category?: String;
    type?: String;
}
