export class User {
  _id?: string;
  fname?: string;
  lname?: string;
  gender?: string;
  username?: string;
  email?: string;
  role?: string;
}
